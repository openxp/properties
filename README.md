# properties library
Add <x-data mixin="properties"/> to your applications site.xml to use.

    <site>
      <x-data mixin="properties"/>
      <config/>
    </site>